import matplotlib.pyplot as plt
import pandas as pd

# [1] Tworzenie dataframe dla danych do analizy
data_wszystkie_parametry_mean = []
data_wszystkie_parametry_std = []
header = ['nazwa', 'filtr', 'peak', 'crest', 'envelope peak', 'kurtoza', 'rms', 'FTF', 'BSF', 'BPO', 'BPI']
data_wszystkie_parametry_mean = pd.DataFrame(data_wszystkie_parametry_mean, columns=header)
data_wszystkie_parametry_std = pd.DataFrame(data_wszystkie_parametry_std, columns=header)

tempdf = []
tempdf = pd.DataFrame(tempdf, columns=['t1', 't2', 't3', 't4'])
# [1]

# [2] wczytanie danych z pliku
df = pd.read_csv("/home/adrian/Videos/DaneLoz.csv", decimal=',', delimiter='\t')
# [2]

# [3] segregacja danych pod wzgledem zmiennej test
data_test1 = df[df.test == 1.0]
data_test2 = df[df.test == 2.0]
data_test3 = df[df.test == 3.0]
data_test4 = df[df.test == 4.0]
# [!3]

# [4] przepisanewszytkich zmiennych do nowego dataframe
data_wszystkie_parametry_mean['nazwa'] = data_test1['nazwa'].reset_index()['nazwa']
data_wszystkie_parametry_mean['filtr'] = data_test1['filtr'].reset_index()['filtr']

data_wszystkie_parametry_std['nazwa'] = data_wszystkie_parametry_mean['nazwa']
data_wszystkie_parametry_std['filtr'] = data_wszystkie_parametry_mean['filtr']

lista_parametrow = ['peak', 'crest', 'envelope peak', 'kurtoza', 'rms', 'FTF', 'BSF', 'BPO', 'BPI']
for zmienna in lista_parametrow:
    tempdf['t1'] = data_test1[zmienna].reset_index()[zmienna]
    tempdf['t2'] = data_test2[zmienna].reset_index()[zmienna]
    tempdf['t3'] = data_test3[zmienna].reset_index()[zmienna]
    tempdf['t4'] = data_test4[zmienna].reset_index()[zmienna]

    data_wszystkie_parametry_mean[zmienna] = tempdf.mean(axis=1)
    data_wszystkie_parametry_std[zmienna] = tempdf.std(axis=1) 
# [!4]

# [5] wybranie wyłącznie wyników dla filtra 8000Hz
data_mean_f8000 = data_wszystkie_parametry_mean[data_wszystkie_parametry_mean.filtr == 8000]
data_std_f8000 = data_wszystkie_parametry_std[data_wszystkie_parametry_std.filtr == 8000]
# [!5]

# [6] rysowanie wykresu zbiorczego ze znormalizowanym zakresem parametrow
for zmienna in lista_parametrow:
    tempdf = tempdf.iloc[0:0]

    tempdf['t1'] = data_mean_f8000['nazwa']
    tempdf['t2'] = data_mean_f8000[zmienna]

    max_value = max(tempdf['t2'])
    tempdf['t2'] = tempdf['t2'] / max_value

    sortowanie = tempdf.sort_values(by=['t2'])

    plt.plot(range(0, 9), sortowanie['t2'], label=zmienna)
# [!6]

# [7] zapisanie wykresu znormalizowanego
plt.legend()
plt.savefig("/home/adrian/analiza_danych/zbiorczy_normalizacja.jpeg")
plt.clf()
# [!7]

# [8] zapisanie poszczegulnych wykresow zmiennych
for zmienna in lista_parametrow:
    tempdf = tempdf.iloc[0:0]

    tempdf['t1'] = data_mean_f8000['nazwa']
    tempdf['t2'] = data_mean_f8000[zmienna]
    tempdf['t3'] = data_std_f8000[zmienna]

    sortowanie = tempdf.sort_values(by=['t2'])

    plt.errorbar(sortowanie['t1'], sortowanie['t2'], sortowanie['t3'], linestyle='None', marker = '^')
    plt.xticks(sortowanie['t1'], sortowanie['t1'], rotation = 'vertical')
    plt.title(zmienna + " 8000Hz")
    plt.savefig("/home/adrian/analiza_danych/wyniki_szczegulowe/wykres std" + zmienna + " 8000Hz.jpeg")
    plt.clf()
# [8]

# [9] analiza rank - MFidali
rank_v1 = data_wszystkie_parametry_mean.copy()

for zmienna in lista_parametrow:
    rank_v1[zmienna] =pd.qcut(rank_v1[zmienna],10,labels=False)

rank_v1 = rank_v1[rank_v1.filtr == 8000]
rank_v1 = rank_v1.drop(columns='filtr')
rank_v1['sumRank'] = rank_v1.sum(axis=1, skipna=True)
rank_v1 = rank_v1.sort_values(by=['sumRank'])
#print(rank_v1)i
# [9] 

# [10] analiza rank - AKrol
header_new = ['nazwa', 'filtr']
for zmienna in lista_parametrow:
    header_new.append(zmienna + ' mean')
    header_new.append(zmienna + ' std')

rank_v2=[]
rank_v2 = pd.DataFrame(rank_v2, columns=header_new)

rank_v2['nazwa'] = data_wszystkie_parametry_mean['nazwa']
rank_v2['filtr'] = data_wszystkie_parametry_mean['filtr']

for zmienna in lista_parametrow:
    rank_v2[zmienna + ' mean'] = data_wszystkie_parametry_mean[zmienna]
    rank_v2[zmienna + ' std'] = data_wszystkie_parametry_std[zmienna]

rank_v2 = rank_v2[rank_v2.filtr == 8000]
rank_v2 = rank_v2.drop(columns='filtr')
rank_v2['rankMean'] = rank_v2.mean(axis=1, skipna=True)
rank_v2['rankStd'] = rank_v2.std(axis=1, skipna=True)
rank_v2['RANK'] = rank_v2['rankMean']*(rank_v2['rankStd']+1)
rank_v2 = rank_v2.sort_values(by=['RANK'])
# [10] 

# [11] zapis rankow do xlsx
with pd.ExcelWriter('/home/adrian/analiza_danych/wyniki.xlsx') as writer:
    rank_v1.to_excel(writer, sheet_name='rank Fidali')
    rank_v2.to_excel(writer, sheet_name='rank Krol')
# [11] 
