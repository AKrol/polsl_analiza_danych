import csv
import numpy as np

with open('/home/adrian/Videos/dane_U.csv', mode='w') as writerU:
	writerU = csv.writer(writerU, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)

	with open('/home/adrian/Videos/dane_L.csv', mode='w') as writerL:
		writerL = csv.writer(writerL, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)

		with open('/home/adrian/Videos/dane_N.csv', mode='w') as writerN:
			writerN = csv.writer(writerN, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)

			with open('/home/adrian/Videos/wynik_v1i.txt') as csvfile:
				spamreader = csv.reader(csvfile, delimiter='	', quotechar='|')

				for row in spamreader:
					if row[0] != "nazwa":
						for x in range (1, 12):
							row[x]=row[x].replace(",", ".")
							row[x]=float(row[x])

						if row[0][0]=="U":
							writerU.writerow(row)
						elif row[0][0]=="N":
							writerN.writerow(row)
						elif row[0][0]=="L":
							writerL.writerow(row)
