import csv
import numpy as np
import matplotlib.pyplot as plt

test1=[]
test2=[]
test3=[]
test4=[]

test_500=[]
test_1000=[]
test_2000=[]
test_5000=[]
test_8000=[]

wynik_500=[]
wynik_1000=[]
wynik_2000=[]
wynik_5000=[]
wynik_8000=[]

header=['nazwa', 'test', 'filtr', 'peak', 'crest', 'envelope peak', 'kurtoza', 'rms', 'FTF', 'BSF', 'BPO', 'BPI']

testy=[]
i=1
nrrys=1
with open('/home/adrian/Videos/dane_U.csv') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)



        for row in spamreader:
                if row[1] == '1.0':
                        test1.append(row)
                elif row[1] == '2.0':
                        test2.append(row)
                elif row[1] == '3.0':
                        test3.append(row)
                elif row[1] == '4.0':
                        test4.append(row)

        testy.append(['test1', test1])
        testy.append(['test2', test2])
        testy.append(['test3', test3])
        testy.append(['test4', test4])
        
        
        
        for test in testy:
                nazwa_testu=test[0]
                dane_testow=test[1]
                
                print(nazwa_testu)
                
                test_500=[]
                test_1000=[]
                test_2000=[]
                test_5000=[]
                test_8000=[]
                
                for row in dane_testow:
                        if row[2] == '500.0':
                                test_500.append([nazwa_testu,row])
                        elif row[2] == '1000.0':
                                test_1000.append([nazwa_testu,row])
                        elif row[2] == '2000.0':
                                test_2000.append([nazwa_testu,row])
                        elif row[2] == '5000.0':
                                test_5000.append([nazwa_testu,row])
                        elif row[2] == '8000.0':
                                test_8000.append([nazwa_testu,row])
                               
                for zmienna in range(3,12):
                
                        wynik_500=[]
                        for wiersz in test_500:
                                nazwa_testu=wiersz[0]+' f=500 ' + header[zmienna]
                                wynik_500.append([nazwa_testu, wiersz[1][0], wiersz[1][zmienna]])
                        
                        wynik_1000=[]          
                        for wiersz in test_1000:
                                nazwa_testu=wiersz[0]+' f=1000 ' + header[zmienna]
                                wynik_1000.append([nazwa_testu, wiersz[1][0], wiersz[1][zmienna]])

                        wynik_2000=[]           
                        for wiersz in test_2000:
                                nazwa_testu=wiersz[0]+' f=2000 ' + header[zmienna]
                                wynik_2000.append([nazwa_testu, wiersz[1][0], wiersz[1][zmienna]])
                        
                        wynik_5000=[]                 
                        for wiersz in test_5000:
                                nazwa_testu=wiersz[0]+' f=5000 ' + header[zmienna]
                                wynik_5000.append([nazwa_testu, wiersz[1][0], wiersz[1][zmienna]])
                                
                        wynik_8000=[]
                        for wiersz in test_8000:
                                nazwa_testu=wiersz[0]+' f=8000 ' + header[zmienna]
                                wynik_8000.append([nazwa_testu, wiersz[1][0], wiersz[1][zmienna]])
                                  
                        wyniki_zebrane=[]
                        #wyniki_zebrane.append([wynik_500, wynik_1000, wynik_2000, wynik_5000, wynik_8000])

                        wyniki_zebrane.append([wynik_8000])

                        for wyniki in wyniki_zebrane:
                                for pomiary in wyniki:
                                        wartosc=[]
                                        label=[]        
                                        
                                        nazwa_wykresu = [row[0] for row in pomiary]
                                        wartosc = [row[2] for row in pomiary]
                                        label = [row[1] for row in pomiary]
                                        
                                        wartosc_float=[]
                                        for num in wartosc:
                                                wartosc_float.append(float(num))
                                        
                                        a=[]
                                        i=1
                                        max_value=max(wartosc_float)

                                        while len(wartosc_float)>0:
                                                index = wartosc_float.index(min(wartosc_float))
                                                a.append([i, label[index], wartosc_float[index]/max_value])
                                                wartosc_float.remove(wartosc_float[index])
                                                label.remove(label[index])                                
                                                i+=1
                                         
                                        plt.plot([row[0] for row in a],[row[2] for row in a])
                                        plt.title(nazwa_wykresu[0])
                                        #plt.show()
                                        #plt.savefig("/home/adrian/analia_danych/wyniki/"+nazwa_wykresu[0]+".jpeg")
                                        nrrys+=1
#                                        plt.clf()
plt.savefig("/home/adrian/test.jpeg")

