import csv 
import os
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

lozysko = 'N12'
typ = 'AMP' #widmo aplitudowe czy po odbiewni AMP ENV
test = '1' #numer testu 1,2,3,4
source = "/home/adrian/Videos/VibDiag2019" #sciezka z danymi
imagePath = "/home/adrian" #sciezka do zapisu obrazu
fmin = 100 #zakres czestotliwosci min
fmax = 2000 #zakres czestotliwosci max

header = ['freq', 'test 1', 'test 2', 'test 3', 'test 4']
data_wszystkie=[]
data_wszystkie = pd.DataFrame(data_wszystkie, columns=header)

plik = source + "/" + lozysko + " " + typ + " 0.csv"

df = pd.read_csv(plik, decimal=',', delimiter='\t')

plt.plot(df['freq'],df['test ' + test], linewidth=0.5)
#plt.show()

plt.xlim(fmin, fmax)	


plt.savefig(imagePath + "/" + lozysko + " " + typ  + " fmin" + str(fmin) + " fmax" + str(fmax) + ".png" , bbox_inches='tight', pad_inches=0, dpi=300)